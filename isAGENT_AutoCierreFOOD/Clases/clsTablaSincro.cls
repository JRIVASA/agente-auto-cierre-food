VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsTablaSincro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mVarNombre                                                                          As String
Private mCampos                                                                             As Collection
Private mVarTipo                                                                            As eTipoTablaSincro

Property Get Nombre() As String
    Nombre = mVarNombre
End Property

Property Get Tipo() As eTipoTablaSincro
    Tipo = mVarTipo
End Property

Property Get Campos() As Collection
    Set Campos = mCampos
End Property

Friend Function AgregarTabla(pNombre As String, pTipo As eTipoTablaSincro, pCamposB, pCamposEx)
    mVarNombre = pNombre
    mVarTipo = pTipo
    Set mCampos = New Collection
    AgregarCampos pCamposB, etblBusqueda
    AgregarCampos pCamposEx, etblExcepcion
    Set AgregarTabla = Me
End Function

Private Sub AgregarCampos(pCampos, pTipo As eTipoCampo)
    
    Dim mCamposTbl As Variant
    Dim i As Integer
    Dim mCampoTbl As clsCampos
    
    mCamposTbl = Split(pCampos, "|")
    
    For i = 0 To UBound(mCamposTbl)
        Set mCampoTbl = New clsCampos
        mCampos.Add mCampoTbl.AgregarCampo(CStr(mCamposTbl(i)), pTipo)
    Next i
    
End Sub

Private Function EsCampoEx(pNombre As String) As Boolean
    
    Dim mCampo As clsCampos
    
    For Each mCampo In mCampos
        If CadenasIguales(mCampo.Nombre, pNombre) And mCampo.Tipo = etblExcepcion Then
            EsCampoEx = True
            Exit Function
        End If
    Next
    
End Function

