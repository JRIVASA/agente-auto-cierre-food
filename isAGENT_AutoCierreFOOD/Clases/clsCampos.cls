VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsCampos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mVarNombre                                                                          As String
Private mVarTipo                                                                            As eTipoCampo

Property Get Nombre() As String
    Nombre = mVarNombre
End Property

Property Get Tipo() As eTipoCampo
    Tipo = mVarTipo
End Property

Friend Function AgregarCampo(pCampo As String, pTipo As eTipoCampo) As clsCampos
    mVarNombre = pCampo
    mVarTipo = pTipo
    Set AgregarCampo = Me
End Function
