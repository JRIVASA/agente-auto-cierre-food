VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsAgenteActualiza"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mArchivos                                                                               As Collection
Private mvarCarpetaDatos                                                                        As String
Private mvarCarpetaProcesado                                                                    As String
Private mvarCarpetaNoProcesado                                                                  As String
Private mTablasSincro                                                                           As Collection

Event Estatus(pEstatus As String)



Public Sub ActualizarDatos(pCn As ADODB.Connection, pTblSincronizar As Collection, pRuta As String, pSucursal As String, Optional pSucPrincipal As Boolean = False, Optional pSoloPos As Boolean = False)
    Dim mTbl As clsTablasSincronizar
    Dim miArchivo As String
    Dim mTabla As String, mCorrelativo As String
    Dim mManejaPos As Boolean
    
    
    Set mTablasSincro = pTblSincronizar
    mvarCarpetaDatos = pRuta
    If pSucPrincipal Then
        mvarCarpetaProcesado = pRuta & "Procesados\"
        mvarCarpetaNoProcesado = pRuta & "NoProcesados\"
    Else
        mvarCarpetaProcesado = pRuta & "SUC" & pSucursal & "\Procesados\"
        mvarCarpetaNoProcesado = pRuta & "SUC" & pSucursal & "\NoProcesados\"
    End If
    
    VerificarCarpetas
    
    mManejaPos = SucursalManejaPos(pCn)
    
    Set mArchivos = BuscarArchivosActualizar(mvarCarpetaDatos)
    For n = 1 To mArchivos.Count
        miArchivo = mArchivos(n)
        If miArchivo <> "" Then
             mTabla = "": mCorrelativo = ""
             If NombreTablaArchivo(miArchivo, mTabla, mCorrelativo) Then
                Set mTbl = BuscarTablaSincro(mTabla)
                If Not mTbl Is Nothing Then
                    RaiseEvent Estatus("Actualizando " & mTbl.Descripcion)
                    Select Case mTbl.Proceso
                        Case 0
                            ActualizarRsSucursal pCn, mTbl, IIf(mManejaPos, etaAdmPos, etaSoloAdm), mCorrelativo, pSucursal, "tr_pendiente_codigo", "ma_codigos"
                        Case 1, 4
                            ActualizarRsSucursal pCn, mrbl, IIf(mManejaPos, etaAdmPos, etaSoloAdm), mCorrelativo, pSucursal
                        Case 2, 3
                            If Not pSucPrincipal Then ActualizarRsSucursal pCn, mTbl, etaSoloAdm, mCorrelativo, pSucursal
                    End Select
                End If
            End If
         End If
         
     Next
     
End Sub

Private Sub ActualizarRsSucursal(pCnSucursal As ADODB.Connection, pTbl As clsTablasSincronizar, pTipo As eTipoActualizacion, pCorrelativo As String, pSucursal As String, Optional pTblSec, Optional pTblSecMa, Optional pRuta As String = "", Optional pTblVad20 As Boolean = True)
    Dim mTbl As clsTablaSincro
    Dim mTblPend As clsTablaSincro
    Dim mTblTr As clsTablaSincro
    
    Set mTbl = BuscarTablaActualizar(pTbl, etblMaestra)
    Set mTblPend = BuscarTablaActualizar(pTbl, etblPendiente)
    Set mTblTr = BuscarTablaActualizar(pTbl, etblTransaccion)
    
    If mTblTr Is Nothing Then '
        If pTipo = etaAdmPos Or pTipo = etaSoloAdm Then
            ActualizarTabla pCnSucursal, mTbl.Nombre, mTblPend.Nombre, mTbl.Campos, pCorrelativo, pSucursal
        End If
        If pTipo <> etaSoloAdm Then
            If pTblVad20 Then ActualizarTabla pCnSucursal, mTbl.Nombre, mTblPend.Nombre, mTbl.Campos, pCorrelativo, pSucursal, "VAD20"
            InsertarPendientePos pCnSucursal, mTblPend.Nombre, mTblPend.Campos, pCorrelativo, pSucursal
        End If
            
        If Not IsMissing(pTblSec) Then
            If pTipo = etaAdmPos Or pTipo = etaSoloAdm Then
                ActualizarTabla pCnSucursal, CStr(pTblSecMa), CStr(pTblSec), mTbl.Campos, pCorrelativo, pSucursal
            End If
            If pTipo <> etaSoloAdm Then
                If pTblVad20 Then ActualizarTabla pCnSucursal, CStr(pTblSecMa), CStr(pTblSec), mTbl.Campos, pCorrelativo, pSucursal, "VAD20"
                InsertarPendientePos pCnSucursal, CStr(pTblSec), mTbl.Campos, pCorrelativo, pSucursal
            End If
        End If
    Else
        'aqui no valido por que solo es adm
        If pTipo <> etaSoloPos Then
            If Trim(UCase(mTblPend.Nombre)) Like "TR_PEND_ODC*" Then
                ActualizarOdc pCnSucursal, mTblPend, mTblTr, pCorrelativo, pSucursal
            ElseIf Trim(UCase(mTblPend.Nombre)) Like "TR_PEND_USUARIO*" Then
                ActualizarUsuario pCnSucursal, mTblPend, pCorrelativo, pSucursal
            End If
        End If
            
    End If
End Sub

Private Sub ActualizarTabla(pCnSucursal As ADODB.Connection, pTblMa As String, pTblPend As String, pCampos As Collection, pCorrelativo As String, pSucursal As String, pPosTr As Boolean, pPosMa As Boolean)
    Dim mRsPend As ADODB.Recordset
    Dim mSql As String, mBD As String
    Dim mArrayProcesado  As clsArreglo
    Dim mArrayNoProcesado As clsArreglo
    
    On Error Resume Next
    Set mArrayProcesado = New clsArreglo
    Set mArrayNoProcesado = New clsArreglo
    DoEvents
    RaiseEvent Estatus("Actualizando " & pTblMa)
    Set mRsPend = AbrirRsActualizar(NombreArchivo(pTblPend, pCorrelativo, mvarCarpetaDatos))
    If Not mRsPend Is Nothing Then
        Do While Not mRsPend.EOF
            'aqui actualizar ,insertar
            
            mBD = IIf(IsMissing(pBd), "VAD10", CStr(pBd))
            If ActualizarRegistro(pCnSucursal, mRsPend, pTblMa, pCampos, pSucursal, pCorrelativo, mBD) Then
                mArrayProcesado.AddItem mRsPend.Bookmark
            Else
                mArrayNoProcesado.AddItem mRsPend.Bookmark
            End If
            mRsPend.MoveNext
        Loop
        If mArrayNoProcesado.NumItems > 0 Then
            CopiarArchivosProcesado mRsPend, pTblPend, pCorrelativo, mArrayProcesado, mArrayNoProcesado
        Else
            CopiarArchivo NombreArchivo(pTblPend, pCorrelativo, mvarCarpetaDatos), NombreArchivo(pTblPend, pCorrelativo, mvarCarpetaProcesado), True
        End If
        mRsPend.Close
    End If
    RaiseEvent Estatus("Proceso Culminado")
End Sub


Private Sub InsertarPendientePos(pCn As ADODB.Connection, pTablaPend As String, pCampos As Collection, pCorrelativo As String, pSucursal As String)
    Dim mRsPend As ADODB.Recordset
    Dim mSql As String
    Dim mRsCajas As ADODB.Recordset
    
    On Error Resume Next
    Set mRsPend = AbrirRsActualizar(NombreArchivo(pTablaPend, pCorrelativo, mvarCarpetaDatos))
    If Not mRsPend Is Nothing Then
        Do While Not mRsPend.EOF
            If LCase(pTablaPend) Like "*pendiente*" Then
                InsertarRegistro pCn, mRsPend, pTablaPend, pCampos, pCorrelativo, pSucursal
            Else
                Set mRsCajas = New ADODB.Recordset
                mRsCajas.Open "Select distinct c_codigo from vad20.dbo.ma_caja ", pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
                Do While Not mRsCajas.EOF
                    If Not InsertarRegistro(pCn, mRsPend, pTablaPend, pCampos, pCorrelativo, pSucursal, mrscaja!c_codigo) Then
                        Exit Do
                    End If
                    mRsCajas.MoveNext
                Loop
            End If
            mRsPend.MoveNext
        Loop
        mRsPend.Close
    End If
End Sub




Private Function BuscarTablaSincro(pTabla As String) As clsTablasSincronizar
    Dim mTbl As clsTablasSincronizar
    Dim mTabla As clsTablaSincro
    
    For Each mTbl In mTablasSincro
        For Each mTabla In mTbl.Tablas
            If CadenasIguales(pTabla, mTabla.Nombre) Or (CadenasIguales(pTabla, "tr_pendiente_codigo") And CadenasIguales(mTabla.Nombre, "tr_pendiente_prod")) Then
                Set BuscarTablaSincro = mTbl
                Exit Function
            End If
        Next
    Next
End Function

