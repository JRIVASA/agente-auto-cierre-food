VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsAgente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private CnAdm                                                                       As ADODB.Connection
Private CnPos                                                                       As ADODB.Connection

Private mCorrelativo                                                                As String

Private POSList                                                                     As Collection
Private mSucursales                                                                 As Collection
Private mTablasSincro                                                               As Collection

Event EstatusAgente(pEstatus As String)
Event IniciarLista()
Event Terminar()

Public Enum ModoProcesamientoRs
    mPRsVariable
    mPRsActualizar
    mPRsEliminar
End Enum

Private Const RegistrosPendientesPorCorrida = "-_-_-_-_-"
Private Const RegistrosPendientesPorLote = ".........."
Private Const RegistrosPendientesFallidos = "!!!!!!!!!!"
Private Const RegistrosNuevos = vbNullString
Private RegistrosPorLote As Long ' mEstrucConex.nRegistrosLote

'******************************************** Metodo Publico *******************************************************

Private Sub TestSP()
    
'    mConexion.Execute ProcedimientoSQL_Limpiar, TmpRows
'
'    mConexion.Execute ProcedimientoSQL_Dependencia1, TmpRows
'
'    mConexion.Execute ProcedimientoSQL_Base, TmpRows
'
'    Set CmdTemp = New ADODB.Command
'    Set CmdTemp.ActiveConnection = mConexion
    
'    CmdTemp.Parameters.Append CmdTemp.CreateParameter("P1", adVarChar, adParamInput, 20, "CUALQUIER COSA")
'    'CmdTemp.Parameters.Append CmdTemp.CreateParameter("@O1", adVarChar, adParamOutput, "433443")
'    CmdTemp.Parameters.Append CmdTemp.CreateParameter("O1", adVarChar, adParamOutput, 2000)
'    CmdTemp.CommandType = adCmdStoredProc
'    CmdTemp.CommandText = "TestReturnValue#"
    
'    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@DBName", adVarChar, adParamInput, 255, "VAD20")
'    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@Schema", adVarChar, adParamInput, 255, "DBO")
'    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@TableName", adVarChar, adParamInput, 255, "TR_CAJA")
'    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@IncludeConstraints", adInteger, adParamInput, 1, 1)
'    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@IncludeIndexes", adInteger, adParamInput, 1, 1)
'    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@NewTableSchema", adVarChar, adParamInput, 255, "DBO")
'    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@NewTableName", adVarChar, adParamInput, 255, "TR_CAJA_TMP")
'    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@UseSystemDataTypes", adInteger, adParamInput, 1, 0)
'    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@CreateTableAndCopyData", adInteger, adParamInput, 1, 0)
'    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@Script", adVarChar, adParamOutput, (2 ^ 16), 0)
    
'    CmdTemp.CommandText = "CopyTable#"
'
'    CmdTemp.Execute
    
'    MsgBox "Return Value: " & CmdTemp.Parameters("@Script")
    
    'Debug.Print CopyTableScript(mConexion, "VAD20", "TR_CAJA", "TR_CAJA_COPY", "VAD10", , , True, True, False)
    
End Sub

Public Function CopyTableScript(pCn As ADODB.Connection, ByVal pDBName As String, ByVal pTable As String, _
ByVal pNewTableName As String, Optional ByVal pTargetDBName As String = "", Optional ByVal pTableSchema = "dbo", _
Optional ByVal pNewTableSchema = "dbo", Optional ByVal pIncludeConstraints As Boolean = True, _
Optional ByVal pIncludeIndexes As Boolean = False, Optional ByVal pUseSystemDataTypes As Boolean = False, _
Optional ByVal pCopyData As Boolean = False) As String
    
    On Error GoTo Error
    
    Dim TmpRows, CmdTemp
    
    If pTargetDBName = vbNullString Then pTargetDBName = pDBName
    
    pCn.Execute ProcedimientoSQL_Limpiar, TmpRows
    
    pCn.Execute ProcedimientoSQL_Dependencia1, TmpRows
    
    pCn.Execute ProcedimientoSQL_Base, TmpRows
    
    Set CmdTemp = New ADODB.Command
    Set CmdTemp.ActiveConnection = pCn
    
    CmdTemp.CommandType = adCmdStoredProc
    
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@DBName", adVarChar, adParamInput, 255, pDBName)
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@Schema", adVarChar, adParamInput, 255, pTableSchema)
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@TableName", adVarChar, adParamInput, 255, pTable)
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@IncludeConstraints", adInteger, adParamInput, 1, IIf(pIncludeConstraints, 1, 0))
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@IncludeIndexes", adInteger, adParamInput, 1, IIf(pIncludeIndexes, 1, 0))
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@NewTableSchema", adVarChar, adParamInput, 255, pNewTableSchema)
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@NewTableName", adVarChar, adParamInput, 255, pNewTableName)
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@NewDBName", adVarChar, adParamInput, 255, pTargetDBName)
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@UseSystemDataTypes", adInteger, adParamInput, 1, IIf(pUseSystemDataTypes, 1, 0))
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@CreateTableAndCopyData", adInteger, adParamInput, 1, IIf(pCopyData, 1, 0))
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@Script", adVarChar, adParamOutput, (2 ^ 16), 0)
    
    CmdTemp.CommandText = "CopyTable#"
    
    CmdTemp.Execute
    
    CopyTableScript = CmdTemp.Parameters("@Script")
    
    Exit Function
    
Error:
    
End Function

Public Sub IniciarAgente(pCnAdm As ADODB.Connection, pCnPos As ADODB.Connection)
    
    Set CnAdm = pCnAdm
    Set CnPos = pCnPos
    
    'MostrarForm
    
    'RegistrosPorLote = mEstrucConex.nRegistrosLote
    
    RealizarCierres
    
End Sub

Private Sub MostrarForm()
    Set frmAgente = Nothing
    Set frmAgente.fCls = Me
    frmAgente.Show
    VentanaVisible frmAgente, False
End Sub

'********************************************** Metodos Agente ********************************************************

Private Function ProcesarRs(pRs As ADODB.Recordset, pConexion As ADODB.Connection, pDB As String, pTablaDestino As String, _
pCamposPK As Variant, Optional ByVal pCamposOmitir = Empty, Optional ByVal nCaja As String = "", _
Optional ModoProcesarRs As ModoProcesamientoRs = mPRsVariable, _
Optional ByVal TransaccionLocal As Boolean = True) As Boolean
    
    On Error GoTo Error
    
    Dim mTrans As Boolean
    
    If Not (pRs.BOF And pRs.EOF) Then pRs.MoveFirst
    
    If TransaccionLocal Then
        pConexion.BeginTrans
        mTrans = True
    End If
    
    Do While Not pRs.EOF
        
        Dim ActualizarRegistro As Boolean, pRsDestino As ADODB.Recordset, mSQL As String, mCriterio As String, mField As ADODB.Field
        
        If ModoProcesarRs = mPRsVariable Then ' Determinar si es actualización o eliminación por medio del campo.
            If Not IsNull(SafeItem(pRs, "TipoCambio", Null)) Then
                ActualizarRegistro = (pRs!TIPOCAMBIO <> 1)
            ElseIf Not IsNull(SafeItem(pRs, "Tipo_Cambio", Null)) Then
                ActualizarRegistro = (pRs!TIPO_CAMBIO <> 1)
            ElseIf Not IsNull(SafeItem(pRs, "Estatus", Null)) Then
                ActualizarRegistro = (pRs!Estatus <> 1)
            End If
        ElseIf ModoProcesarRs = mPRsActualizar Then
            ActualizarRegistro = True '
        ElseIf ModoProcesarRs = mPRsEliminar Then
            ActualizarRegistro = False
        End If
        
        Set pRsDestino = New ADODB.Recordset
        
        If ActualizarRegistro Then
            
            mCriterio = CriterioPK(pRs, pCamposPK)
            
            If nCaja <> "" Then mCriterio = mCriterio & GetLines & IIf(Len(mCriterio) > 0, "AND", "WHERE") & " (" & "n_Caja" & " = '" & nCaja & "')"
            
            If Len(mCriterio) <= 0 Then mCriterio = GetLines & "WHERE 1 = 2 -- NUEVO REGISTRO" ' Si no se especifica PK, hacer INSERT.
            
            mSQL = "SELECT * FROM [" & pDB & "].[dbo].[" & pTablaDestino & "]" & mCriterio
            
            pRsDestino.Open mSQL, pConexion, adOpenKeyset, adLockPessimistic
            
            If pRsDestino.EOF Then pRsDestino.AddNew
            
            For Each mField In pRsDestino.Fields
                If ValidarCampo(mField.Name, pCamposOmitir) Then
                    If ExisteCampoRs(pRs, mField.Name) Then
                        SafePropAssign mField, "Value", SafeItem(pRs.Fields, mField.Name, Null) ' Error Proof
                    End If
                End If
            Next
            
            If nCaja <> "" Then pRsDestino!n_Caja = nCaja
            
            pRsDestino.Update
            
        Else
            
            mCriterio = CriterioPK(pRs, pCamposPK)
            
            If nCaja <> "" Then mCriterio = mCriterio & GetLines & IIf(Len(mCriterio) > 0, "AND", "WHERE") & " (" & "n_Caja" & " = '" & nCaja & "')"
            
            mSQL = "DELETE FROM [" & pDB & "].[dbo].[" & pTablaDestino & "]" & mCriterio
            
            pConexion.Execute mSQL, Rows
            
        End If
        
        pRs.MoveNext
        
    Loop
    
    If mTrans Then
        pConexion.CommitTrans
    End If
    
    ProcesarRs = True
    
    Exit Function
    
Error:
    
    If mTrans Then
        pConexion.RollbackTrans
    End If
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    
    EscribirLog "ProcesarRs:" & mErrorDesc, mErrorNumber
    
    GrabarErrorBD "ProcesarRs", mErrorDesc, mErrorNumber, ErrActualizando, , , pTablaDestino, , , Now
    
End Function

Private Function ValidarCampo(pCampo As String, pArrayCampos As Variant) As Boolean
    
    Dim CampoExcepcion
    
    For Each CampoExcepcion In AsEnumerable(pArrayCampos)
        If CadenasIguales(pCampo, CStr(CampoExcepcion)) Then
            Exit Function
        End If
    Next
    
    ValidarCampo = True
    
End Function

Private Function CriterioPK(pRs As ADODB.Recordset, pArrayCampos As Variant) As String
    
    On Error Resume Next
    
    Dim Campo, Valor, ValorCompuesto, Criterio As String
    
    For Each Campo In AsEnumerable(pArrayCampos)
        
        Campo = Split(CStr(Campo), ":", 2)
        
        If UBound(Campo) = 0 Then
            
            Campo = Campo(0)
            Criterio = Criterio & GetLines & IIf(Len(Criterio) > 0, "AND", "WHERE") & " (" & Campo & " = '" & pRs.Fields(Campo).Value & "')"
            
        Else
            
            Valor = Split(CStr(Campo(1)), "+")
            
            For Each Valor In AsEnumerable(Valor)
                
                Valor = Split(CStr(Valor), "VAL->", 2)
                
                If UBound(Valor) = 0 Then
                    Valor = Valor(0)
                    ValorCompuesto = ValorCompuesto & CStr(pRs.Fields(Valor).Value)
                Else
                    Valor = Valor(1)
                    ValorCompuesto = ValorCompuesto & Valor
                End If
                
            Next
            
            Criterio = Criterio & GetLines & IIf(Len(Criterio) > 0, "AND", "WHERE") & " (" & Campo(0) & " = '" & ValorCompuesto & "')"
            
        End If
        
    Next
    
    CriterioPK = Criterio
    
End Function

Private Function EjecutarCierre(ConexionAdm As Object, ConexionPos As Object, _
Caja As String, CodigoCajero As String, DescripcionCajero As String, _
Grupo As String, Localidad As String, Organizacion As String, Turno As Long, _
Usuario As String) As Boolean
    
    Dim mCls As Object
    
    Set mCls = CreateObject("Cierres.clase_posmonitor")
    
    'Dim DLLCierresTeclado As FrmEvents
    
    'Set DLLCierresTeclado = New FrmEvents
    
    'SafePropAssign mCls, "ObjTeclado", DLLCierresTeclado
    SafePropAssign mCls, "AutoDeclarar", True
    'SafePropAssign mCls, "AnchoImpresoraSetup", AnchoImpresoraEspecifico
    SafePropAssign mCls, "HabilitarModoAgente", True
    
    EjecutarCierre = mCls.EjecutarCierreTotal(ConexionAdm, ConexionPos, Caja, CodigoCajero, DescripcionCajero, Grupo, Localidad, Organizacion, Turno, Usuario)
    
    Set mCls = Nothing
    
End Function

'Private Function ObtenerDatosProximoTurno(pUltimoCierre As Double) As ADODB.Recordset
Private Function ObtenerDatosProximoTurno(pFechaCierre As Date) As ADODB.Recordset
    
    On Error GoTo Error
    
    Dim mRs As ADODB.Recordset, mSQL As String
    
    Set mRs = New ADODB.Recordset
    
    mRs.CursorLocation = adUseClient
    
    'mSQL = "SELECT * FROM TR_CAJA WHERE (cs_Estado = 'A' OR cs_Estado = 'C') AND (ID > " & pUltimoCierre & ") ORDER BY ds_Fecha_Inicio"
    mSQL = "SELECT * FROM TR_CAJA WHERE (cs_Estado = 'A' OR cs_Estado = 'C') AND (ds_Fecha_Inicio > '" & FechaBD(pFechaCierre, FBD_FULL) & "') ORDER BY ds_Fecha_Inicio"
    
    mRs.Open mSQL, CnPos, adOpenStatic, adLockReadOnly
    
    If Not mRs.EOF Then
        Set mRs.ActiveConnection = Nothing
        Set ObtenerDatosProximoTurno = mRs
    End If
    
    Exit Function
    
Error:
    
    EscribirLog "Error al Obtener Datos del Próximo Turno. Información Adicional: " & Err.Description, Err.Number
    
End Function

Private Function ExistenMesasAbiertas() As Boolean
    
    Dim SQL As String, Rs As New ADODB.Recordset
    
    SQL = "SELECT cs_CodigoServicio FROM MA_CONSUMO WHERE (bs_Consolidada = 0)"
    
    'Debug.Print SQL
    
    Rs.Open SQL, CnPos, adOpenForwardOnly, adLockReadOnly
    
    If Rs.EOF Then
        ExistenMesasAbiertas = False
    Else
        ExistenMesasAbiertas = True
    End If
    
End Function

Private Function ExistenMesasAbiertasPOE(pCodUsuario As String) As Boolean
    
    Dim SQL As String, Rs As New ADODB.Recordset
    
    SQL = "SELECT cs_CodigoServicio FROM MA_CONSUMO WHERE (bs_Consolidada = 0) AND (LEFT(cs_CodigoMesa, 1) <> 'L' AND cs_CodigoMesero = '" & pCodUsuario & "')"
    
    'Debug.Print SQL
    
    Rs.Open SQL, CnPos, adOpenForwardOnly, adLockReadOnly
    
    If Rs.EOF Then
        ExistenMesasAbiertasPOE = False
    Else
        ExistenMesasAbiertasPOE = True
    End If
    
End Function

Friend Sub RealizarCierres()
    
    On Error GoTo Error
    
    Dim mRsCierre As ADODB.Recordset, Verificacion As Boolean, Relacion As String, mCierreID As Double, mCierreFecha As Date
    
    Set mRsCierre = ObtenerDatosProximoTurno(CDate(0)) 'mCierreID)
    
    While Not mRsCierre Is Nothing
        With mRsCierre
            Verificacion = False
            If UCase(!cs_Estado) = UCase("C") Then
                Verificacion = True
            Else
                If UCase(!cs_Caja) = UCase("POE") Then
                    Verificacion = Not ExistenMesasAbiertasPOE(!cs_Cajero)
                Else
                    Verificacion = Not ExistenMesasAbiertas
                End If
            End If
            If Verificacion Then
                Relacion = CnPos.Execute("SELECT c_Relacion FROM MA_CAJA WHERE c_Codigo = '" & !cs_Caja & "'")!c_Relacion
                If Relacion <> vbNullString Then
                    If Not EjecutarCierre(CnAdm, CnPos, !cs_Caja, !cs_Cajero, !cs_Nombre_Cajero, Relacion, !cs_Localidad, !cs_Organizacion, !ns_Turno, !cs_Cajero) Then
                        EscribirLog "Error al Realizar Cierre. Datos del Cierre: " & GetLines & PrintRecordset(mRsCierre), 0
                    End If
                End If
            End If
            'mCierreID = !ID
            mCierreFecha = !ds_Fecha_Inicio
        End With
        Set mRsCierre = ObtenerDatosProximoTurno(mCierreFecha) 'mCierreID)
    Wend
    
    Exit Sub
    
Error:
    
    EscribirLog "Error al Realizar Cierres. Información Adicional: " & Err.Description, Err.Number
    
End Sub

Private Sub AsignarEstatus(pEstatus As String): RaiseEvent EstatusAgente(pEstatus): End Sub

Private Function ObtenerRegistrosMonitor() As ADODB.Recordset
    
    On Error GoTo Error
    
    Dim mSQL As String, mRs As ADODB.Recordset
    mSQL = "SELECT * FROM [VAD20].[dbo].[TR_CAJA]"
    
    Set mRs = New ADODB.Recordset
    
    mRs.CursorLocation = adUseClient
    
    mRs.Open mSQL, mConexion, adOpenKeyset, adLockReadOnly, adCmdText
    
    Set mRs.ActiveConnection = Nothing
    
    Set ObtenerRegistrosMonitor = mRs
    
    Exit Function
    
Error:
    
End Function
